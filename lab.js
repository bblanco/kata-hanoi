var OutputHanoiConsole = function(){
	this.outerMargins = 2;
	this.railsMargins = 4;
	this.numberOfDiscs = 8;
	this.railWidth = 16;
};

OutputHanoiConsole.prototype._renderMargins = function(margin) {
	var output = "", i = 0;
	for (; i < margin; i++){
			output += " ";
	}
	return output;
};

OutputHanoiConsole.prototype._renderDisk = function(disk){
	var output = "", i = 0; 
	var indexInitDisk = (this.railWidth - disk) / 2;
	var indexEndDisk = indexInitDisk + disk;
	var indexMiddle = this.railWidth / 2 - 1;
	for (; i < this.railWidth; i++){
			if (i < indexInitDisk || i >= indexEndDisk) {
				output += " ";
			} else {
				output += "=";	
			}
			if (i == indexMiddle){
				output += "|";	
			}
	}
	return output;
};

OutputHanoiConsole.prototype._renderEmpty = function(){
	return this._renderDisk(0);
};

OutputHanoiConsole.prototype._renderRow = function(hanoiState, row){
	var i = 0, railsLen = hanoiState.length;
	var outputRow = "";
	outputRow += this._renderMargins(this.outerMargins);
	for (; i < railsLen; i++){
		var rail = hanoiState[i];
		if (rail.length > row){
			outputRow += this._renderDisk(rail[row]);
		} else {
			outputRow += this._renderEmpty();
		}
		outputRow += this._renderMargins(this.railsMargins);
	}
	outputRow += this._renderMargins(this.outerMargins);
	return outputRow;
};

OutputHanoiConsole.prototype._renderState = function(hanoiState, opts){
	var i = 0;
	for (;i < this.numberOfDiscs; i++){
		var outputRow = this._renderRow(hanoiState, i);
		console.log(outputRow);
	}
};



OutputHanoiConsole.prototype.render = function(hanoiState, opts){
	this._renderState(hanoiState);
};

var outputHanoi = new OutputHanoiConsole();
outputHanoi.render([[16, 14 , 12, 10, 8, 6, 4, 2], [], []]);


// ex hanoiState: [
//								  [8, 7 , 6, 5, 4, 3, 2, 1] --> [16, 14 , 12, 10, 8, 6, 4, 2]
//                  []
//                  []
//                 ]



